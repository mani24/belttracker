from django.urls import path
from . import views


app_name = 'belttracker'

urlpatterns = [
    path('', views.index, name='index'),
    path('clear/<int:id>/', views.clear, name='clear-belt'),
    path('reset/', views.reset_all, name='reset-belts'),
    path('reset/<int:id>/', views.reset, name='reset-belt'),
]
